Mateusz Soliwodzki

 # RateGame

> RateGame jest to serwis internetowy, dzięki któremu komentujemy oraz oceniamy gry .

  - Ruby : 2.2.0
  - Ruby on Rails : 4.2.0
  - Niestandardowe gem'y:
    * gem 'sass-rails', '>= 5.0.0'
    * gem 'bootstrap-sass', '~> 3.2.0'
    * gem 'will_paginate', '~> 3.0.7'

##### Aby odpalić lokalnie... 
.
```sh
bundle install
rake db:migrate
rake db:seed
```
##### Aby odpalić na heroku... 
.
```sh
heroku login
heroku create
git push heroku master
heroku run rake db:migrate
heroku run rake db:seed
```

##### Aby zalogować się jako administrator... 
.
```sh
$ login: admin
$ pass: admin
```

Wersja 1.0 
