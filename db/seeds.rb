# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

game_list = [
  [ "GTA V", 2015 ],
  [ "Witcher 2", 2009 ],
  [ "Sims 3", 2010 ],
  [ "GTA III", 2001 ],
  [ "Far Cry 4", 2014 ],
  [ "Battlefield 4", 2013 ],
  [ "GTA III", 2001 ],
  [ "Mafia II", 2010 ],
  [ "Metro: Last Light", 2013 ],
    
  [ "Mortal Kombat X", 2015 ],
  [ "PoEternity", 2015 ],
  [ "TES V", 2011 ],
  [ "DA: Inkwizycja", 2014 ],
  [ "Dying Light", 2014 ],
  [ "World of Tanks", 2010 ],
  [ "The Crew", 2014 ],
  [ "WWE 2K15", 2014 ],  
  [ "Risen 2", 2013 ],
]

game_list.each do |game|
  Game.create( :name => game[0], :year => game[1] )
end
