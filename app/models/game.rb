class Game < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates :name, presence: true,
                    length: { minimum: 3 }
def self.search(query)
  where("name like ?", "%#{query}%") 
end
    
end

