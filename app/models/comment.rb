class Comment < ActiveRecord::Base
  belongs_to :game 
  validates :mark, presence: true,
                    numericality: { greater_than:  0, less_than_or_equal_to: 10 }

end
